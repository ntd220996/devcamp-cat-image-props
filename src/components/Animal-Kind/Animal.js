import { Component } from 'react';
import imagesCAT from '../../assets/images/cat.jpg'

class Animal extends Component {
    render() {
        let  { kind } = this.props;
        return (
            <div style={{textAlign: 'center'}}>
                <h1 >Mèo</h1>
                { kind ? ( <img src={imagesCAT} />) : ( <p> MEOW NOT FOUND :) </p> ) }
            </div>
        )
    }
}

export default Animal