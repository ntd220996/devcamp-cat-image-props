import Animal from "./components/Animal-Kind/Animal";


function App() {
  return (
    <div>
      <Animal kind = 'cat' />
    </div>
  );
}

export default App;
